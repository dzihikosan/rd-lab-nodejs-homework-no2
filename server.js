const express = require('express');
const dotenv = require('dotenv');
const morgan = require('morgan');
const mongoose = require('mongoose');
const app = express();

dotenv.config({path: 'config.env'});


mongoose.connect(process.env.Mongo_URL, {useNewUrlParser: true},
    () => console.log('MongoDB connected'));

app.use(express.json());

const PORT = process.env.PORT;

app.use(morgan('common'));

app.get('/', (req, res) => {
  res.send('ok');
});

app.use('/', require('./server/routes/router'));


app.listen(PORT, () => {
  console.log(`Server is running on http://localhost:${PORT}`);
});
