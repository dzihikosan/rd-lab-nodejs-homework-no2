const User = require('../model/user');
const Notes = require('../model/note');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');


exports.create = async (req, res) => {
  const userExist = await User.findOne({username: req.body.username});

  if (!req.body.username && !req.body.password) {
    res.status(400).json({message: '\'Username\' and \'Password are required'});
  } else if (!req.body.username) {
    res.status(400).json({message: '\'Username\' is required'});
  } else if (userExist) {
    res.status(400).json({message: 'Username already exists'});
  } else if (!req.body.password) {
    res.status(400).json({message: '\'Password\' is required'});
  } else {
    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(req.body.password, salt);


    const user = new User({
      username: req.body.username,
      password: hashedPassword,
    });
    try {
      await user.save();
      res.status(200).json({message: 'Success'});
    } catch (err) {
      res.status(500).json({message: err.message});
    }
  }
};

exports.login = async (req, res) => {
  const user = await User.findOne({username: req.body.username});

  if (!req.body.username && !req.body.password) {
    return res.status(400).json(
        {message: '\'Username\' and \'Password are required'});
  } else if (!req.body.username) {
    return res.status(400).json({message: '\'Username\' is required'});
  } else if (!user) {
    return res.status(400).json({message: 'Username does not exist'});
  } else if (!req.body.password) {
    return res.status(400).json({message: '\'Password\' is required'});
  }
  const validPass = await bcrypt.compare(req.body.password, user.password);
  if (!validPass) {
    return res.status(400).json({message: 'Invalid password'});
  }
  const token = jwt.sign({_id: user._id}, process.env.TOKEN_SECRET);
  try {
    res.status(200).json({message: 'Success',
      jwt_token: token,
    });
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};

exports.changePassword = async (req, res) => {
  const user = await User.findById(req.user);

  if (!req.body.oldPassword && !req.body.newPassword) {
    return res.status(400).json(
        {message: '\'oldPassword\' and \'newPassword\' are required'});
  } else if (!req.body.oldPassword) {
    return res.status(400).json({message: '\'oldPassword\' is required'});
  } else if (!req.body.newPassword) {
    return res.status(400).json({message: '\'newPassword\' is required'});
  }
  const validPass = await bcrypt.compare(req.body.oldPassword, user.password);
  if (!validPass) {
    return res.status(400).json({message: 'Invalid password'});
  }
  try {
    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(req.body.newPassword, salt);
    await User.updateOne({_id: req.user},
        {
          $set: {password: hashedPassword},
        });
    res.status(200).json({message: 'Password changed successfully'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};


exports.deleteUser = async (req, res) => {
  try {
    await User.remove({_id: req.user});
    res.status(200).json({message: 'Profile deleted successfully'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};

exports.userInfoGet = async (req, res) => {
  const me = await User.findOne({_id: req.user});
  try {
    res.status(200).json({user: {
      _id: me._id,
      username: me.username,
      createdDate: me.createdDate}});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};

exports.notesGet = async (req, res) => {
  try {
    const perPg = Math.max(0, +req.query.limit || 0);
    const notes = await Notes.find({userId: req.user}).skip(
        +req.query.offset || 0).limit(+req.query.limit || 0);
    res.status(200).json({
      offset: +req.query.offset || 0,
      limit: +req.query.limit || 0,
      count: Math.floor(perPg / +req.query.offset || 0),
      notes: notes});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};

exports.notesPost = async (req, res) => {
  if (!req.body.text) {
    return res.status(400).json({message: 'Please specify \'text\' parameter'});
  }
  const note = new Notes({
    userId: req.user,
    text: req.body.text,
  });
  try {
    await note.save();
    res.status(200).json({message: 'Success'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};

exports.notesIdGet = async (req, res) => {
  try {
    const note = await Notes.findById(req.params.id);
    if (note.userId !== req.user._id) {
      return res.status(400).json({message: 'Access to the note denied'});
    }
    res.status(200).json({note: note});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};

exports.notesIdPatch = async (req, res) => {
  try {
    const note = await Notes.findById(req.params.id);
    if (note.userId !== req.user._id) {
      return res.status(400).json({message: 'Access to the note denied'});
    }
    const checked = note.completed;
    await Notes.updateOne({_id: req.params.id},
        {
          $set: {completed: !checked && true},
        });
    res.status(200).json({message: 'Success'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};


exports.notesIdPut = async (req, res) => {
  try {
    const note = await Notes.findById(req.params.id);
    if (!req.body.text) {
      return res.status(400).json({
        message: 'Please specify \'text\' parameter'});
    }
    if (note.userId !== req.user._id) {
      return res.status(400).json({message: 'Access to the note denied'});
    }
    await Notes.findByIdAndUpdate(req.params.id, req.body);
    res.status(200).json({message: 'Success'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};

exports.notesIdDelete = async (req, res) => {
  try {
    const aNote = await Notes.findById(req.params.id);
    if (aNote.userId !== req.user._id) {
      return res.status(400).json({message: 'Access to the note denied'});
    }
    await Notes.remove({_id: req.params.id});
    res.status(200).json({message: 'Success'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};

