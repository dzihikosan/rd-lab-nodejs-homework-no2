const express =require('express');
const route = new express.Router();
const verify = require('../routes/verifyToken');
const controller = require('../controller/controller');

route.post('/api/auth/register', controller.create);
route.post('/api/auth/login', controller.login);


route.get('/api/users/me', verify, controller.userInfoGet);
route.patch('/api/users/me', verify, controller.changePassword);
route.delete('/api/users/me', verify, controller.deleteUser);

route.get('/api/notes', verify, controller.notesGet);
route.post('/api/notes', verify, controller.notesPost);

route.get('/api/notes/:id', verify, controller.notesIdGet);
route.patch('/api/notes/:id', verify, controller.notesIdPatch);
route.put('/api/notes/:id', verify, controller.notesIdPut);
route.delete('/api/notes/:id', verify, controller.notesIdDelete);

module.exports=route;
