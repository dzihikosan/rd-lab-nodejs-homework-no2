const mongoose = require('mongoose');

const notesSchema = new mongoose.Schema({
  userId: {
    type: String,
  },
  completed: {
    type: Boolean,
    default: false,
  },
  text: {
    type: String,
  },
  createdDate: {
    type: Date,
    default: Date.now,
  },
}, {
  versionKey: false,
});

module.exports = mongoose.model('Notes', notesSchema);
